import React from "react";

type Props = {
    // store: Store;
    isMobile: boolean;
    // teamSlug: string;
    // redirectMessage?: string;
};

class Assets extends React.Component<Props> {
    public render() {
        const {isMobile} = this.props;
        return (
            <p>Your assets are here</p>
        )
    }
}

export default Assets;