import type { ReactElement } from "react";
import AssetsContainer from "../components/assets/assetsContainer";
import ViewTitle from "../components/common/viewTitle";
import TradeViewLayout from "../components/tradeViewLayout";

type Props = {
    // store: Store;
    isMobile: boolean;
    // teamSlug: string;
    // redirectMessage?: string;
};

let assets = [
    {assetName: "Asset 1", symbol: "ASS1", id: "1", nShares: 100, latestPrice: 0.01},
    {assetName: "Asset 2", symbol: "ASS2", id: "2", nShares: 200, latestPrice: 0.10},
    {assetName: "Asset 3", symbol: "ASS3", id: "3", nShares: 150, latestPrice: 0.23},
]

export default function TradePage() {
    return (
        <>
            <ViewTitle isMobile={false} title={"Trade"}/>
            <AssetsContainer 
                isMobile={false}
            />
        </>
    )
}

TradePage.getLayout = function getLayout(page: ReactElement) {
    return (
        <TradeViewLayout>
            {page}
        </TradeViewLayout>
    )
}