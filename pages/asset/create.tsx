import React from "react";
import CreateAssetForm from "../../components/assets/createAssetForm";

type Props = {
    // store: Store;
    // discussion: Discussion;
    isMobile: boolean;
};

class Create extends React.Component<Props> {
    public render() {
        return (
            <>
                <CreateAssetForm isMobile={this.props.isMobile} />
            </>
        )
    }
}

export default Create;