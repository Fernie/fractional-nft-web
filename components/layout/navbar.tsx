import { AppBar, Button, IconButton, SvgIcon, Toolbar, Typography } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import { Box } from '@mui/system';
import React from 'react';

export default function Navbar() {
    return (
        <Box sx={{ flexGrow: 1}}>
            <AppBar position="fixed">
                <Toolbar>
                    <IconButton            
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                        S
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        Solvent
                    </Typography>
                    <Button variant="contained" endIcon={<AccountBalanceWalletIcon />}>
                        Connect Wallet
                    </Button>
                </Toolbar>
            </AppBar>
            <Toolbar />
        </Box>
    )
}