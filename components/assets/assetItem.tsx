import { Button, Grid, Paper, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

type Props = {
    // store: Store;
    // discussion: Discussion;
    assetName: String;
    latestPrice: number;
    nShares: number;
    symbol: String;
    isMobile: boolean;
};

class AssetItem extends React.Component<Props> {
    public render() {
        const { isMobile } = this.props;

        return(
            <Paper
                style={{
                    margin: '10px 10px 5px 0px',
                    padding: '8px',
                    border: 'none',
                }}
            >
                <Box sx={{ display: 'flex', flexDirection: 'column'}}>
                    <Grid container spacing={2}>
                        <Grid item xs={4}>
                            <Typography variant="h6" component="div">
                                {this.props.assetName} {`($${this.props.symbol})`}
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography variant="subtitle1" component="div">
                                # Shares
                            </Typography>
                            <Typography variant="subtitle2" component="div">
                                {this.props.nShares}
                            </Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <Typography variant="subtitle1" component="div">
                                Latest Share/STX sale
                            </Typography>
                            <Typography variant="subtitle2" component="div">
                                {this.props.latestPrice} STX
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Button variant="contained">See bids</Button>
                        </Grid>
                    </Grid>
                    
                </Box>
            </Paper>
        )
    }
}

export default AssetItem;