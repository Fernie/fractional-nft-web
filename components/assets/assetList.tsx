import React from "react";

import AssetItem from "./assetItem";

type Props = {
    // store: Store;
    isMobile: boolean;
    // teamSlug: string;
    // redirectMessage?: string;
};

let assets = [
    {assetName: "Asset 1", symbol: "ASS1", id: "1", nShares: 100, latestPrice: 0.01},
    {assetName: "Asset 2", symbol: "ASS2", id: "2", nShares: 200, latestPrice: 0.10},
    {assetName: "Asset 3", symbol: "ASS3", id: "3", nShares: 150, latestPrice: 0.23},
]

class AssetList extends React.Component<Props> {
    public render() {
        const {isMobile} = this.props;
        return (
            <React.Fragment>
                {assets &&
                    assets.map((asset) => {
                        return (
                            <AssetItem
                                key={asset.id}
                                assetName={asset.assetName}
                                nShares={asset.nShares}
                                latestPrice={asset.latestPrice}
                                symbol={asset.symbol}
                                isMobile={this.props.isMobile}
                            />
                        );
                    })
                }
            </React.Fragment>
        )
    }
}

export default AssetList;