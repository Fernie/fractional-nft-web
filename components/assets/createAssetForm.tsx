import { Button, FormLabel, InputLabel, Paper, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

type Props = {
    // store: Store;
    // discussion: Discussion;
    isMobile: boolean;
};

class CreateAssetForm extends React.Component<Props> {
    public render() {
        const { isMobile } = this.props;

        return(
            <Box
                component="form"
                autoComplete="off"
                sx={{
                    width: 600,
                }}
            >
                <Paper
                    elevation={4}
                    sx={{
                        '& > :not(style)': { m: 1, width: '560px' },
                        padding: 2,
                    }}
                >
                    <Typography
                        variant="h4"
                        align="center"
                        gutterBottom={true}
                    >
                        Create an asset
                    </Typography>
                    <InputLabel>Symbol*</InputLabel>
                    <TextField
                        error={null ? true : false}
                        required
                        id="symbol"
                        helperText="10 characters max."
                    />
                    <InputLabel>Name*</InputLabel>
                    <TextField
                        error={null ? true : false}
                        required
                        id="symbol"
                        helperText="50 characters max."
                    />
                    <InputLabel>Total number of shares*</InputLabel>
                    <TextField
                        error={null ? true : false}
                        required
                        id="n-shares"
                        helperText="10 min. "
                    />
                    {/* to be sold to the public */}
                    <InputLabel>Float shares*</InputLabel>
                    <TextField
                        error={null ? true : false}
                        required
                        id="float-shares"
                    />
                    <Button
                        variant="contained"
                        disabled={true ? true : false}
                    >
                        Create asset
                    </Button>
                </Paper>

            </Box>
        )
    }
}

export default CreateAssetForm;