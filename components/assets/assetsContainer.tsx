import { InputAdornment, Paper, TextField } from "@mui/material";
import React from "react";
import AssetList from "./assetList";
import { styled, alpha } from '@mui/material/styles';
import SearchIcon from '@mui/icons-material/Search';
import InputBase from '@mui/material/InputBase';
import { Box } from "@mui/system";


type Props = {
    // store: Store;
    isMobile: boolean;
    // teamSlug: string;
    // redirectMessage?: string;
};

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  }));
  
  const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }));
  
  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch',
      },
    },
  }));


class AssetsContainer extends React.Component<Props> {
    public render() {
        let { isMobile } = this.props;

        return (
            <>
                <Paper
                    sx={{
                        p: 1
                    }}
                    elevation={1}
                >
                    <Box
                        flexGrow={1}
                        sx={{
                            border: 2,
                            mx: "auto",
                            textAlign: "center",
                            mb: 2,
                            mt: 2,
                        }}
                    >
                        
                        <TextField
                            id="input-with-icon-textfield"
                            label="Search"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                    <SearchIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                            sx={{
                                width: 400
                            }}
                        />
                    </Box>
                    <AssetList isMobile={isMobile}/>
                </Paper>
            </>
        )
    }
}

export default AssetsContainer;