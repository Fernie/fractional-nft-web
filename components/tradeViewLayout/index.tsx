import { Grid } from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';
import LateralBar from './lateralBar'

type Props = {
    children: React.ReactNode;
    isMobile?: boolean;
    firstGridItem?: boolean;
    // store?: Store;
    // teamRequired?: boolean;
};

class TradeViewLayout extends React.Component<Props> {
    render() {
        const { children, isMobile, firstGridItem} = this.props;
        return (
            <Grid
                container
                spacing={0.5}
                direction="row"
                alignItems="stretch"
            >
                <Grid item xs={2}
                    sx={{
                        // border: 2
                    }}
                >
                    <LateralBar/>
                    
                </Grid>
                <Grid item xs={10}>
                    <main>{children}</main>
                </Grid>

            </Grid>
            
        )
    }
}

export default TradeViewLayout;