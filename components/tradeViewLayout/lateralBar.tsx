import { Divider, Drawer, List, ListItem, ListItemIcon, ListItemText, Typography } from "@mui/material";
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import WorkIcon from '@mui/icons-material/Work';
import React from "react";

type Props = {
    isMobile?: boolean;
};


class LateralBar extends React.Component<Props> {
    public render() {
        const { isMobile } = this.props;
        return (
            <>
                <List
                    sx={{
                        m: 2,
                    }}
                >
                    <ListItem>
                        <ListItemText primary="Options" />
                    </ListItem>
                    <Divider />
                    <ListItem
                        button
                        sx={{
                            m: 1,
                            borderRadius: 2
                        }}
                    >
                        <ListItemIcon>
                            <WorkIcon />
                        </ListItemIcon>
                        <ListItemText primary={"My Assets"} />
                    </ListItem>
                    <ListItem
                        button
                        sx={{
                            m: 1,
                            borderRadius: 2
                        }}
                    >
                        <ListItemIcon>
                            <AttachMoneyIcon />
                        </ListItemIcon>
                        <ListItemText primary={"Buy/Sell"} />
                    </ListItem>
                </List>
            </>
        )
    }
}

export default LateralBar;