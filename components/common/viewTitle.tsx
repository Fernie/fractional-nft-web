import { AppBar, Paper, Typography } from "@mui/material";
import React from "react";

type Props = {
    // store: Store;
    isMobile: boolean;
    title: string;
    // teamSlug: string;
    // redirectMessage?: string;
};


class ViewTitle extends React.Component<Props> {
    public render() {
        let { isMobile, title } = this.props;
        return (
            <Paper
                variant="outlined"
                elevation={0}
            >
                <Typography
                    variant="h5"
                    component="div"
                    sx={{
                        flexGrow: 1,
                        margin: 1
                    }}
                >
                    {title}
                </Typography>
            </Paper>
        )
    }
}

export default ViewTitle;